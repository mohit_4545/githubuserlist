
//
//  Created by Mohit on 2/19/20.
//  Copyright © 2019 Mohit. All rights reserved.
//

import Foundation
 
class githubUser: Decodable {
    var id: Int
    var login: String
    var node_id: String
    var avatar_url: String
    var gravatar_id: String
    var url: String
    var html_url: String
    var followers_url: String
    var gists_url: String
    var starred_url: String
    var subscriptions_url: String
    var organizations_url: String
    var repos_url: String
    var events_url: String
    var received_events_url: String
    var type: String
    var site_admin: Bool

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case login = "login"
        case node_id = "node_id"
        case avatar_url = "avatar_url"
        case gravatar_id = "gravatar_id"
        case url = "url"
        case html_url = "html_url"
        case followers_url = "followers_url"
        case gists_url = "gists_url"
        case starred_url = "starred_url"
        case subscriptions_url = "subscriptions_url"
        case organizations_url = "organizations_url"
        case repos_url = "repos_url"
        case events_url = "events_url"
        case received_events_url = "received_events_url"
        case type = "type"
        case site_admin = "site_admin"
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        login = try container.decode(String.self, forKey: .login)
        node_id = try container.decode(String.self, forKey: .node_id)
        avatar_url = try container.decode(String.self, forKey: .avatar_url)
        gravatar_id = try container.decode(String.self, forKey: .gravatar_id)
        url = try container.decode(String.self, forKey: .url)
        html_url = try container.decode(String.self, forKey: .html_url)
        followers_url = try container.decode(String.self, forKey: .followers_url)
        gists_url = try container.decode(String.self, forKey: .gists_url)
        gists_url = try container.decode(String.self, forKey: .gists_url)
        starred_url = try container.decode(String.self, forKey: .starred_url)
        subscriptions_url = try container.decode(String.self, forKey: .subscriptions_url)
        organizations_url = try container.decode(String.self, forKey: .organizations_url)
        repos_url = try container.decode(String.self, forKey: .repos_url)
        events_url = try container.decode(String.self, forKey: .events_url)
        received_events_url = try container.decode(String.self, forKey: .received_events_url)
        type = try container.decode(String.self, forKey: .type)
        site_admin = try container.decode(Bool.self, forKey: .site_admin)
    }
}



