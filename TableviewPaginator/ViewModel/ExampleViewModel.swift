
//
//  Created by Mohit on 2/19/20.
//  Copyright © 2019 Mohit. All rights reserved.
//

import Foundation

protocol ExampleViewModelProtocol: class {
    func userFetched(success: Bool, userCount: Int)
}

class ExampleViewModel {
    private weak var delegate: ExampleViewModelProtocol?
    private var users: [githubUser] = [githubUser]()

    init(delegate: ExampleViewModelProtocol) {
        self.delegate = delegate
    }

    func numberOfRows() -> Int {
        return users.count
    }

    func getUser(at index: Int) -> githubUser? {
        guard index < users.count else {
            return nil
        }
        return users[index]
    }


    
    
    
    func fetchUsers(offset: Int, limit: Int, shouldAppend: Bool) {
        let page = offset / limit
        print("page: \(page+1), per-page=\(limit)")
        APIClient.fetchUser(page: page+1, perPage: limit) { (result) in
            switch result.response?.statusCode {
            case 200?:
                if  let data = result.data,
                    let userResponseModel = try? JSONDecoder().decode([githubUser].self, from: data) {

                    if shouldAppend == false {
                        self.users.removeAll()
                    }
                    self.users.append(contentsOf: userResponseModel)
                    self.delegate?.userFetched(success: true, userCount: userResponseModel.count)
                } else {
                    self.delegate?.userFetched(success: false, userCount: 0)
                }
            default:
                self.delegate?.userFetched(success: false, userCount: 0)
            }
        }
    }
    
    
    
}
