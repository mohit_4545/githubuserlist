
//
//  Created by Mohit on 2/19/20.
//  Copyright © 2019 Mohit. All rights reserved.
//

import Alamofire

class APIClient {

    static func fetchUser(page: Int, perPage: Int, completion: @escaping (DataResponse<Any>) -> Void) {

        Alamofire.request(Constants.fetchUser(page: page, perPage: perPage),
                          method: .get,
                          parameters: nil,
                          encoding: JSONEncoding.default,
                          headers: nil).responseJSON(completionHandler: completion)
    }

}
