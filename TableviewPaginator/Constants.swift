
//
//  Created by Mohit on 2/19/20.
//  Copyright © 2019 Mohit. All rights reserved.
//

import Foundation
import UIKit

struct Constants {

    static let tintColor = UIColor.init(red: 40.0/255.0, green: 116.0/255.0, blue: 239.0/255.0, alpha: 1.0)

    struct DOMAIN {
//        static let reques = "https://reqres.in"
        static let reques = "https://api.github.com"
    }

    struct ROUTE {
//        static let fetchUser = "/api/users?page=%d&per_page=%d"
        static let fetchUser = "/users?since=%d"
         
    }

    static func fetchUser(page: Int, perPage: Int) -> String {
        let url = DOMAIN.reques + ROUTE.fetchUser
        return String.init(format: url, perPage, page)
    }
}
