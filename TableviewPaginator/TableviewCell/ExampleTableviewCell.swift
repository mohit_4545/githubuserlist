
//
//  Created by Mohit on 2/19/20.
//  Copyright © 2019 Mohit. All rights reserved.
//

import UIKit
import SDWebImage

class ExampleTableviewCell: UITableViewCell {

    @IBOutlet weak var mViewUserProfileImage: UIImageView!
    @IBOutlet weak var mViewUserFullName: UILabel!
    @IBOutlet weak var mViewsiteadmin: UILabel!

    func customize(user: githubUser?) {

        mViewUserProfileImage.image = nil
        mViewUserFullName.text = ""
        mViewsiteadmin.text = ""
        guard let user = user else {
            return
        }

        mViewUserFullName.text = "GitUser: " + user.login + ",   Type: " + user.type
        mViewsiteadmin.text = "Status: \(user.site_admin)"
        
        if let url = URL.init(string: user.avatar_url) {
            mViewUserProfileImage.sd_setImage(with: url, completed: nil)
        }
    }

}
